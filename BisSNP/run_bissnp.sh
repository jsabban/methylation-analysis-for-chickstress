#!/bin/bash
#SBATCH -J BisSNP
#SBATCH -e /home/jsabban/work/BisSNP/work/BisSNP.err
#SBATCH -o /home/jsabban/work/BisSNP/work/BisSNP.log
#SBATCH -p workq
#SBATCH --workdir=/home/jsabban/work/BisSNP/work/
#SBATCH --mail-type=BEGIN,END,FAIL
#SBATCH --mail-user=jules.sabban@inrae.fr
#SBATCH --export=ALL
#SBATCH --cpus-per-task=4
#SBATCH --mem=70G

## Load modules
module load bioinfo/BisSNP-v1.0.1
module load bioinfo/picard-2.20.7
module load system/Java8
module load system/perl-5.26.3
module load bioinfo/bcftools-1.9

## Paths
dir=/home/jsabban/work/BisSNP/work/
bam=/work2/genphyse/genepi/RRBS/ChickStress/Methylseq/run20200421/results/bismark_alignments/*.bam
vcfsorter=/home/jsabban/work/vcfsorter.pl
bissnp=/home/jsabban/work/GitHub/Bis-tools/Bis-SNP/
fastadir=/work2/genphyse/genepi/RRBS/ChickStress/galgal6/
genome=${fastadir}Gallus_gallus.GRCg6a.dna.toplevel.fa

## Output directories
bamsorted=${dir}bam_sorted/
snpcompressed=${dir}snp_compressed/
cpgcompressed=${dir}cpg_compressed/

## Tests of the existence of directories
if [ ! -d $dir ];then
	echo -e "Creation of the directory : ${dir}"
	mkdir $dir
fi

if [ ! -d $bamsorted ];then
	echo -e "Creation of the directory : ${bamsorted}"
	mkdir $bamsorted
fi

if [ ! -d $snpcompressed ];then
	echo -e "Creation of the directory : ${snpcompressed}"
	mkdir $snpcompressed
fi

if [ ! -d $cpgcompressed ];then
	echo -e "Creation of the directory : ${cpgcompressed}"
	mkdir $cpgcompressed
fi

## Make index of reference genome
java -Xmx4g  -jar /usr/local/bioinfo/src/picard-tools/picard-2.20.7/picard.jar CreateSequenceDictionary \
R=${genome} \
O=${genome}.fai

# Sorting of Gallus gallus VCF
echo -e "\n[Ordering of reference VCF]"
cmd="perl $vcfsorter ${fastadir}Gallus_gallus.GRCg6a.dna.toplevel.dict ${fastadir}gallus_gallus.vcf > ${dir}gallus_gallus_sorted.vcf"
echo -e  "Command executed :\n$cmd"
eval $cmd

for file in $bam; do
	#f=basename "${file}" | cut -d '.' -sf1
	f=$(basename -s .bam ${file})
	indiv=${f:0:7}			# A voir si on s'en sert

	echo -e "\n####################################"
	echo -e "## Analyse of sample : $indiv"
	echo -e "####################################"
	
	## Sort bam file
	echo '[Sorting of BAM file]'
	cmd="java -Xmx4g  -jar /usr/local/bioinfo/src/picard-tools/picard-2.20.7/picard.jar AddOrReplaceReadGroups \
	I=$file \
	O=${bamsorted}${f}_sorted.bam \
	ID=readGroup_name RGLB=readGroup_name RGPL=illumina RGPU=run RGSM=sample_name CREATE_INDEX=true VALIDATION_STRINGENCY=SILENT SORT_ORDER=coordinate"
	echo -e  "Command executed :\n$cmd"
	eval $cmd
	
	## BisSNP : easy_usage
	echo -e "\n[Running of BisSNP]"
	cmd="perl ${bissnp}bissnp_easy_usage.pl \
	${bissnp}Bis-SNP.latest.jar \
	${bamsorted}${f}_sorted.bam \
	${genome} \
	${dir}gallus_gallus_sorted.vcf\
	--rrbs HS \
	--minCT 5 \
	--mem 15 \
	--cytosine CG,1 \
	--outMode 1"
	echo -e  "Command executed :\n$cmd\n"
	eval $cmd
	
	## Compression of the SNP VCF
	echo -e "\n[Compression of the SNP VCF]"
	cmd="bcftools view ${bamsorted}${f}_sorted.snp.raw.vcf -Oz -o ${snpcompressed}${f}_sorted.snp.raw.vcf.gz"
	echo -e  "Command executed :\n$cmd\n"
	eval $cmd
	cmd="bcftools index ${snpcompressed}${f}_sorted.snp.raw.vcf.gz"
	echo -e  "Command executed :\n$cmd\n"
	eval $cmd

	## Compression of the CpG SNP
	echo -e "\n[Compression of the CpG VCF]"
	cmd="bcftools view ${bamsorted}${f}_sorted.cpg.raw.vcf -Oz -o ${cpgcompressed}${f}_sorted.cpg.raw.vcf.gz"
	eval $cmd
	echo -e  "Command executed :\n$cmd\n"
	cmd="bcftools index ${cpgcompressed}${f}_sorted.cpg.raw.vcf.gz"
	echo -e  "Command executed :\n$cmd\n"
	eval $cmd
done

## Merge of all VCF
echo -e "\n[Merge of all VCF]"
cmd="bcftools merge ${snpcompressed}*vcf.gz -Oz -o ${dir}final.vcf.gz"
echo -e  "Command executed :\n$cmd\n"
eval $cmd
