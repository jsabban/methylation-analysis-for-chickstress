#!/usr/bin/perl 

# Prend en argument un fichier GFF et genere un fichier TSS en ne gardant que les genes

# use : ./make_TSS.pl paht_to_gff > output_file

open(GFF, $ARGV[0]);
print("chr\ttss\tstrand\n");
while(<GFF>){
	unless(/^#/) {
		@ligne = split('\t', $_);
		$chr = $ligne[0];
		$type = $ligne[2];
		if($type eq 'gene'){
			$strand = $ligne[6];
			if($strand eq '+'){
				$tss = $ligne[3];
			}
			else{
				$tss = $ligne[4];
			}
			print("$chr\t$tss\t$strand\n");
		}
	}
}
close(GFF)
