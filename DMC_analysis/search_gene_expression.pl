#!/usr/bin/perl 
## Prend en argument un fichier bed contenant l'identifiant des genes a etudier et un tableau de comptage des niveaux d'expression et renvoie en sortie un plot des niveaux d'expression de chaque genes separes par condition

# usage : ./search_gene_expression.pl --rna /work/project/1000rnaseqchick/internal_runs/RpRm/embr/F0_galgal5_V87dec2016_GTFrennes/Results/Summary/RSEM_multimap_summary_RpRm_embr_F0_genes_TPM.tsv --bed /home/jsabban/work/testAnnotDMC/annotation_with_region.bed --col 9 --cond CT,HS --dir /home/jsabban/work/testAnnotDMCCTHS --pool1 249,250,252,254,255,257,258,259,266,267,269,270 --pool2 251,253,256,260,261,262,263,264,265,268

use Getopt::Long;

GetOptions( "rna=s" => \$rna_file, 	# path to RNA file
		"bed=s" => \$bed, 	# path to bed file
		"col=s" => \$col, 	# column where are gene_id in bed file, first column is 0 !
		"cond=s" => \$condition, 	# list of condition separeted by coma
		"dir=s" => \$dir, 	# work directory
		"pool1=s" => \$pool1,		# names of individuals in pool1
		"pool2=s" => \$pool2 		# names of individuals in pool2
);

unless( -e "${dir}/expression") {
   system "mkdir ${dir}/expression/";
}
unless( -e "${dir}/expression/gene_expression.tsv") {
   system "touch ${dir}/expression/gene_expression.tsv";
}


## Recuperation des gene_id
## et ecriture des niveaux de transcription dans un fichier
print STDERR "Ecriture du fichier des niveaux d'expression...\n";
open(BED, "< $bed") or die "cannot open file : BED";
open(EX, "> ${dir}/expression/gene_expression.tsv") or die "cannot open file : ${dir}/expression/gene_expression.tsv";
select EX;
# header of out file
print("Emb_26 \t Emb_27 \t Emb_28 \t Emb_29 \t Emb_43 \t Emb_44 \t Emb_45 \t Emb_46 \t Emb_47 \t Emb_48 \t Emb_10 \t Emb_11 \t Emb_12 \t Emb_13 \t Emb_15 \t Emb_18 \t Emb_19 \t Emb_20 \t Emb_21 \t Emb_22 \t Emb_23 \t Emb_24\n");

@gene_id_list=();
$bed_header=1;
while(<BED>){
	chomp;
	if ($bed_header){
		$bed_header=0;
	}
	else{
		@line=split("\t", $_);
		($gene_id, $gene_name)=split("_", $line[$col]);
		#$gene_id=$gene[0];
		# Check if gene_id already exists, in this case : no grep
		$find=0;
		foreach $id (@gene_id_list){
			if ($id eq $gene_id){
				$find=1;
				last;
			}
		}
		# Make grep only if gene_id is not in gene_id_list, avoid duplication on exit file
		unless ($find){
			push(@gene_id_list, $gene_id);
			$trans=`grep $gene_id $rna_file | awk '{print \$2, \$3, \$4, \$5, \$6, \$7, \$8, \$9, \$10,\$11, \$12, \$13, \$14, \$15, \$16, \$17, \$18, \$19, \$20, \$21, \$22, \$23}'`;
			if(length($trans) != 0) {  	# check if $gene_id is found in $rna_file
				print("$gene_id"."_"."$gene_name \t $trans");
			}
		} # end_unless
	} # end_else
} # end_while

close(EX);
close(BED);
print STDERR "Fait.\n";


## Plotting
print STDERR "Tracage du boxplot et calculs statistiques...\n";
system `Rscript /home/jsabban/work/GitHub/methylation-analysis-for-chickstress/DMC_analysis/boxplot_gene_expression_with_DMC.R --out ${dir}/expression/ --data ${dir}/expression/gene_expression.tsv --names $condition --pool1 $pool1 --pool2 $pool2`;
