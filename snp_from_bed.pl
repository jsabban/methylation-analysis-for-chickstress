#!/usr/bin/perl
# Retire les positions qui ne sont pas des SNP à partir d'un fichier BED
# snp_from_bed.pl all_columns_To_Galgal6.bed > snp.bed
open(BED, $ARGV[0]);
print("#CHROM\tSTART\tEND\tID\tSCORE\tNAME\tSTRAND\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\tFEEDAGENE_Rm-197-0815\tFEEDAGENE_Rm-199-0641\tFEEDAGENE_Rm-202-0624\tFEEDAGENE_Rm-203-0754\tFEEDAGENE_Rm-304-0576\tFEEDAGENE_Rm-342-0819\tFEEDAGENE_Rm-375-0933\n");
$nb_ligne=0;
while(<BED>){
	$nb_ligne += 1;
	unless(/^#/) {
		chomp;
		@line=split("\t", $_);
		$indiv1=$line[13]; @zygot=split(/:/, $indiv1); $zygot1=$zygot[0];
		$indiv2=$line[14]; @zygot=split(/:/, $indiv2); $zygot2=$zygot[0];
		$indiv3=$line[15]; @zygot=split(/:/, $indiv3); $zygot3=$zygot[0];
		$indiv4=$line[16]; @zygot=split(/:/, $indiv4); $zygot4=$zygot[0];
		$indiv5=$line[17]; @zygot=split(/:/, $indiv5); $zygot5=$zygot[0];
		$indiv6=$line[18]; @zygot=split(/:/, $indiv6); $zygot6=$zygot[0];
		$indiv7=$line[19]; @zygot=split(/:/, $indiv7); $zygot7=$zygot[0];
		unless(($zygot1 eq "0/0") && ($zygot2 eq "0/0") && ($zygot3 eq "0/0") && ($zygot4 eq "0/0") && ($zygot5 eq "0/0") && ($zygot6 eq "0/0") && ($zygot7 eq "0/0") ) {
			print("$_\n");
		}
	}
	# Suivis de l'avancement du traitement
	if(($nb_ligne % 500000) == 0) {
		print STDERR "$nb_ligne lignes traitees.\n";	
	}
}
close(BED);
