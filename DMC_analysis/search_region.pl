#!/usr/bin/perl 

# prend en argument un fichier de sortie de bedtools et un GTF et associe a chaque DMC la region dans laquelle elle se trouve

$GTF=$ARGV[1];
open(DMC, $ARGV[0]);
$header=1;
while(<DMC>){
    chomp;
    if($header) {
	print("$_\tregion\n");
	$header=0;
    }
    else{ 
        @line=split('\t', $_);
        $coord_1=$line[7]+1;
        $coord_2=$line[8];
        @reg=split('_', $line[9]);
        $region=$reg[0];
        @grep=`grep $region $GTF | awk '{print \$3, \$4, \$5}'`;
        $nb_elem=@grep;
        for($i=0;$i<$nb_elem;$i++){
            $sous_grep=@grep[$i];
            ($type, $c1, $c2)=split(" ", $sous_grep);
 	    if((int($coord_1)==int($c1)) && (int($coord_2)==int($c2))){
	        $region_dmc=$type;
            }
        }
        print("$_ \t $region_dmc \n");
    } #end_else
} #end_while
close(DMC);
