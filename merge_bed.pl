#!/usr/bin/perl
# prend en entrée deux BED (1) sortie de LiftOver (2) contenant toutes les positions du VCF
# usage :  ./merge_bed.pl mapped allColumn > mapped_2
open(BED1, $ARGV[0]);
open(BED2, $ARGV[1]);
print("#CHROM\tSTART\tEND\tID\tSCORE\tNAME\tSTRAND\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\tFEEDAGENE_Rm-197-0815\tFEEDAGENE_Rm-199-0641\tFEEDAGENE_Rm-202-0624\tFEEDAGENE_Rm-203-0754\tFEEDAGENE_Rm-304-0576\tFEEDAGENE_Rm-342-0819\tFEEDAGENE_Rm-375-0933\n");
$nb_ligne=0;
$bed2=<BED2>; # lecture premiere ligne, contient le header
while(<BED1>){
	$nb_ligne += 1;
	unless(/^#/) {
		chomp;
		($chrom1, $start1, $end1, $name1, $strand1, $ref1, $alt1)=split("\t", $_);
		($c, $num)=split("hr", $chrom1);
		do{
			$bed2=<BED2>;
			chomp($bed2);
			@ligne2=split("\t", $bed2);
			$name2=$ligne2[5];
			($chrom2, $start2, $end2, $id2, $score2, $name2, $strand2, $ref2, $alt2, $qual, $filter, $info2, $format, $feed1, $feed2, $feed3, $feed4, $feed5, $feed6, $feed7)=split("\t", $bed2);
		}while($name1 ne $name2);
		print("$num\t$start1\t$end1\t$id2\t$score2\t$name2\t$strand2\t$ref2\t$alt2\t$qual\t$filter\t$info2\t$format\t$feed1\t$feed2\t$feed3\t$feed4\t$feed5\t$feed6\t$feed7\n");
	}
	# suivis de l'avancement du traitement
	if(($nb_ligne % 500000) == 0) {
		print STDERR "$nb_ligne traitees...\n";	
	}
}
close(BED1);
close(BED2);
